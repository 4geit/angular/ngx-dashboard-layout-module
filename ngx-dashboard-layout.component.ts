import { Component } from '@angular/core';

@Component({
  selector: 'ngx-dashboard-layout',
  template: require('pug-loader!./ngx-dashboard-layout.component.pug')(),
  styleUrls: ['./ngx-dashboard-layout.component.scss']
})
export class NgxDashboardLayoutComponent {
}
