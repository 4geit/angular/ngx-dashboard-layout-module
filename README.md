# @4geit/ngx-dashboard-layout-module [![npm version](//badge.fury.io/js/@4geit%2Fngx-dashboard-layout-module.svg)](//badge.fury.io/js/@4geit%2Fngx-dashboard-layout-module)

---

module for a dashboard-layout based app

## Installation

1. A recommended way to install ***@4geit/ngx-dashboard-layout-module*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-dashboard-layout-module) package manager using the following command:

```bash
npm i @4geit/ngx-dashboard-layout-module --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-dashboard-layout-module
```

2. You need to import the `NgxDashboardLayoutModule` class in whatever module you want in your project for instance `app.module.ts` as follows:

```js
import { NgxDashboardLayoutModule } from '@4geit/ngx-dashboard-layout-module';
```

And you also need to add the `NgxDashboardLayoutModule` module within the `@NgModule` decorator as part of the `imports` list:

```js
@NgModule({
  // ...
  imports: [
    // ...
    NgxDashboardLayoutModule,
    // ...
  ],
  // ...
})
export class AppModule { }
```
