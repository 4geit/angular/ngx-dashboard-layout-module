import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NgxDashboardLayoutComponent } from './ngx-dashboard-layout.component';
import { NgxLoginComponent } from '@4geit/ngx-login-component';

const routes: Routes = [{
  path: '',
  component: NgxDashboardLayoutComponent,
  children: [
    { path: 'login', component: NgxLoginComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgxDashboardLayoutRoutingModule { }
