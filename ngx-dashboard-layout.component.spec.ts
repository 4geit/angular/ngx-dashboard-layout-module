import { TestBed, async } from '@angular/core/testing';

import { NgxDashboardLayoutComponent } from './ngx-dashboard-layout.component';

describe('NgxDashboardLayoutComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NgxDashboardLayoutComponent
      ],
    }).compileComponents();
  }));

  it('should create the account', async(() => {
    const fixture = TestBed.createComponent(NgxDashboardLayoutComponent);
    const account = fixture.debugElement.componentInstance;
    expect(account).toBeTruthy();
  }));

  it(`should have as title 'account works!'`, async(() => {
    const fixture = TestBed.createComponent(NgxDashboardLayoutComponent);
    const account = fixture.debugElement.componentInstance;
    expect(account.title).toEqual('account works!');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(NgxDashboardLayoutComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('account works!');
  }));
});
