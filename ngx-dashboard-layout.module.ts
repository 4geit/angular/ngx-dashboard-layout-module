import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxMarketplaceHeaderModule } from '@4geit/ngx-marketplace-header-component';
import { NgxFooterModule } from '@4geit/ngx-footer-component';
import { NgxLoginModule } from '@4geit/ngx-login-component';

import { NgxDashboardLayoutRoutingModule } from './ngx-dashboard-layout-routing.module';
import { NgxDashboardLayoutComponent } from './ngx-dashboard-layout.component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    NgxMarketplaceHeaderModule,
    NgxFooterModule,
    NgxDashboardLayoutRoutingModule,
    NgxLoginModule
  ],
  declarations: [
    NgxDashboardLayoutComponent
  ]
})
export class NgxDashboardLayoutModule { }
